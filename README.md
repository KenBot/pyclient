PyKenBot
========

Written by Kenneth Endfinger

PyKenBot is a cool Python Client for the KenBot JSON Api

Running PyKenBot
----------------

Look at the [Wiki Page](https://github.com/kaendfinger/PyKenBot/wiki/Running-PyKenBot)!