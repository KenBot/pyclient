from common import *

__author__ = 'Kenneth Endfinger'

printc('Opening Connection to KenBot', 'blue')
json = getData('/')
config = json['configuration']
o('IRC Server: ', config['server'])
o('Nickname: ', config['user'])
o('Command Prefix: ', config['prefix'])
channels = json['channels']
printc('Channels:', 'bright green')
users = getData('/users')
for channel in channels:
    print("	" + channel)
    for user in users[channel]:
        print("             " + user)
printc('Done', 'blue')