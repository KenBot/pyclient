import json
import urllib.request
import urllib
from config import *
import sys

__author__ = 'Kenneth Endfinger'

codeCodes = {
    'black': '0;30', 'bright gray': '0;37',
    'blue': '0;34', 'white': '1;37',
    'green': '0;32', 'bright blue': '1;34',
    'cyan': '0;36', 'bright green': '1;32',
    'red': '0;31', 'bright cyan': '1;36',
    'purple': '0;35', 'bright red': '1;31',
    'yellow': '0;33', 'bright purple': '1;35',
    'dark gray': '1;30', 'bright yellow': '1;33',
    'normal': '0'
}


def init():
    pwdMgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
    pwdMgr.add_password(None, base, username, password)
    handler = urllib.request.HTTPBasicAuthHandler(pwdMgr)
    opener = urllib.request.build_opener(handler)
    urllib.request.install_opener(opener)


def printc(text, color):
    switchColor(color)
    out(text + '\n')
    switchColor('normal')


def switchColor(color):
    if useColor:
        sys.stdout.write("\033[" + codeCodes[color] + "m")


def out(text):
    sys.stdout.write(text)


def o(text, value):
    switchColor('bright green')
    out(text)
    switchColor('normal')
    out(value + '\n')


def getData(path):
    response = urllib.request.urlopen(base + path)
    dat = response.read().decode("utf-8")
    data = json.loads(dat)
    response.close()
    return data


def sendMessage(target, message):
    getData('/sendMessage?target=' + urlEncode(target) + "&message=" + urlEncode(message))


def urlEncode(data):
    return urllib.request.quote(data)


def getUsers(channel):
    return getData('/users?channel=' + urlEncode(channel))[channel]
